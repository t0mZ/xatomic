package com.tomz.xatomic;

/**
 * @author ZHUFEIFEI
 */
public class DynamicDataSourceContextHolder {
    /**
     * 线程级别的私有变量
     */
    private static final ThreadLocal<String> HOLDER = new ThreadLocal<>();

    public static void set(String key) {
        HOLDER.set(key);
    }

    public static void remove() {
        HOLDER.remove();
    }

    public static String get() {
        return HOLDER.get();
    }

}
