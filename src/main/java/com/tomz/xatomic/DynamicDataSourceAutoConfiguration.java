package com.tomz.xatomic;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.tomz.xatomic.conditional.ConditionalOnCollectionProperty;
import com.tomz.xatomic.xa.XaDynamicDataSourceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * @author ZHUFEIFEI
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Configuration
@ConditionalOnCollectionProperty(Constant.DATA_SOURCE_PROPERTY_PREFIX)
@AutoConfigureBefore({DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class})
@Import({XaDynamicDataSourceConfiguration.class, DynamicDataSourceConfiguration.class})
public class DynamicDataSourceAutoConfiguration implements InitializingBean{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Bean
    @ConditionalOnBean(DynamicDataSourceInitializer.class)
    public DynamicDataSourceAspect dataSourceAspect() {
        return new DynamicDataSourceAspect();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Dynamic dataSource auto configuration enabled.");
    }
}
