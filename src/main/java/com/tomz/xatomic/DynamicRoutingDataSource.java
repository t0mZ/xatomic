package com.tomz.xatomic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 数据源路由
 * @author ZHUFEIFEI
 */
public class DynamicRoutingDataSource extends AbstractRoutingDataSource {
    private final Logger log = LoggerFactory.getLogger(getClass());
    @Override
    protected Object determineCurrentLookupKey() {
        log.info("determineCurrentLookupKey => {}", DynamicDataSourceContextHolder.get());
        return DynamicDataSourceContextHolder.get();
    }
}
